//include dht11 library
#include <DHT_U.h>
#include <DHT.h>
//include tft library
#include <TFT.h>
#include <TFT_S6D02A1.h> // Graphics and font library for S6D02A1 driver chip
#include <SPI.h>

TFT_S6D02A1 tft = TFT_S6D02A1();  // Invoke library, pins defined in User_Setup.h
#define TFT_GREY 0x5AEB // New colour
//dht
#define DHTPIN  A0           // DHT11 data pin is connected to Arduino analog pin 0
#define DHTTYPE DHT11        // DHT11 sensor is used
DHT dht11(DHTPIN, DHTTYPE);  // initialize DHT library
//include moving average library
#include <movingAvg.h>
movingAvg avgTemp(10);// temperature
movingAvg avgHum(10);//humidity

void setup(void) {
  Serial.begin(9600);// com serial port
  avgTemp.begin(); // initialize the moving average
  avgHum.begin();// initialize humidity moving average
  tft.init();//initialize tft
  tft.fillScreen(TFT_BLACK);// black screen
  tft.setRotation(1);// orientation screen
  
  // Set "cursor" at top left corner of display (0,0) and select font 2
  // (cursor will move to next line automatically during printing with 'tft.println'
  //  or stay on the line is there is room for the text with tft.print)
  tft.setCursor(5, 5, 2);
  // Set the font colour to be white with a black background, set text size multiplier to 1
  tft.setTextColor(TFT_WHITE, TFT_BLUE);
  tft.setTextSize(2);
  // We can now plot text on screen using the "print" class
  tft.print("P");// print PopSchool!
  delay(300);
  tft.print("o");
  delay(300);

  tft.print("p");
  delay(300);

  tft.print("S");
  delay(300);

  tft.print("c");
  delay(300);

  tft.print("h");
  delay(300);

  tft.print("o");
  delay(300);
  tft.print("o");
  delay(300);
  tft.print("l");
  delay(300);
  tft.println("!");
  delay(300);

  tft.println("Formation");
  tft.println("AgrIot");
  delay(5000);

  // initialize DHT11 sensor
  dht11.begin();
}
char _buffer[7];

void loop() {
  // mise en page du texte first screen
  tft.fillScreen(S6D02A1_BLACK);
  tft.setTextSize(0.9);
  tft.setRotation(1);
  tft.setCursor(22, 2); // move cursor to position (22, 33) pixel
  tft.setTextColor(ST7735_BLUE, ST7735_WHITE); 
  tft.println("PopSchool Willems");
  tft.setCursor(22, 20);
  tft.print("+ DHT11 SENSOR");
  tft.setTextColor(ST7735_GREEN, ST7735_BLACK);     // set text color to green and black background
  tft.setCursor(25, 41);              // move cursor to position (25, 61) pixel
  tft.print("TEMPERATURE =");
  tft.setTextColor(ST7735_YELLOW, ST7735_BLACK);  // set text color to yellow and black background
  tft.setCursor(25, 83);              // move cursor to position (34, 113) pixel
  tft.print("HUMIDITY =");
  tft.setTextSize(1);                 // text size = 2
  // read humidity
  byte humi = dht11.readHumidity();
  // read temperature
  byte temp = dht11.readTemperature();

  // print temperature (in °C)
  sprintf(_buffer, "%02u.0", temp); // calculate temp
  tft.setTextColor(ST7735_WHITE, ST7735_BLACK);  // set text color to red and black background
  tft.setCursor(29, 58);
  tft.print(_buffer);
  tft.drawCircle(59, 58, 2, ST7735_WHITE);  // print degree symbol ( ° )
  tft.setCursor(49, 58);
  tft.print("C");

  // print humidity (in %)
  sprintf(_buffer, "%02u.0 %%", humi); // calculate humidity
  tft.setTextColor(ST7735_CYAN, ST7735_BLACK);  // set text color to cyan and black background
  tft.setCursor(29, 103);
  tft.print(_buffer);

  delay(3000);    // wait before second screen (average)
// read on serial monitor
  Serial.print("TEMPERATURE =");
  Serial.println(temp);
  

  Serial.print("HUMIDITY =");
  Serial.println(humi);
  
//read average temp and hum on serial monitor
  int avgt = avgTemp.reading(temp); // calculate the moving average
  Serial.print("average temperature ");
  Serial.print(avgt);                   // print the individual reading
  Serial.println("°C");
  delay(2000);

  int avgh = avgHum.reading(humi);             // calculate the moving average
  Serial.print("average humidity ");
  Serial.print(avgh);                   // print the individual reading
  Serial.println("% ");
  delay(2000);
  
// mise en page second screen
// print average data
  tft.fillScreen(S6D02A1_BLACK);

  tft.setTextColor(TFT_BLUE, TFT_WHITE);
  tft.setRotation(1);
  tft.setTextSize(1.5);
  tft.setCursor(25, 15); // move cursor to position (22, 33) pixel
  tft.println("AVERAGE DATA =");
  //print average temperature on second screen
  tft.setTextSize(0.9);
  tft.setTextColor(ST7735_GREEN, ST7735_BLACK);     // set text color to green and black background
  tft.setCursor(15, 40);
  tft.print("AVG TEMPERATURE =");
  tft.setTextColor(ST7735_WHITE, ST7735_BLACK);  // set text color to red and black background
  tft.setCursor(25, 53);
  tft.println(avgt);
  tft.drawCircle(59, 58, 2, ST7735_WHITE);  // print degree symbol ( ° )
  tft.setCursor(42, 53);
  tft.print(".C");
//print average humidity on second creen
  tft.setTextColor(ST7735_YELLOW, ST7735_BLACK);  // set text color to yellow and black background
  tft.setCursor(15, 85);
  tft.print("AVG HUMIDITY =");
  tft.setTextColor(ST7735_CYAN, ST7735_BLACK);  // set text color to cyan and black background
  tft.setCursor(25, 100);
  tft.println(avgh);
 // delay before return to first screen
  delay(3000);

}
